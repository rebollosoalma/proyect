// variable que contiene las coordenadas y el ID del mapa
var mymap = L.map('mainMap').setView([25.4148354,-100.9687603], 15);

// función que permite el acceso a la APi del mapa desde mapbox
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    minZoom: 1,
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiYWxtYXJlYm9sbG9zbyIsImEiOiJja2J4emlraDMwdXo0MnNuc256c3VheXJhIn0.Bd-XjiWSj2pio_mTF35m9Q'
}).addTo(mymap);

// agregar points o marcadores de ubicación
var marker = L.marker([25.4148354,-100.9687603]).addTo(mymap);
var marker2 = L.marker([25.4127907,-100.9640611]).addTo(mymap);

//function para pop up que indique en donde se hizo click
var popup = L.popup();

function onMapClick(e) {
    popup
        .setLatLng(e.latlng)
        .setContent("Haz hecho click en: " + e.latlng.toString())
        .openOn(mymap);
}

mymap.on('click', onMapClick);